import asyncio
import sys
from concurrent.futures import ThreadPoolExecutor, ProcessPoolExecutor, as_completed
from random import randrange, choices
from string import ascii_lowercase
from time import sleep, perf_counter
from functools import reduce
from datetime import datetime
import aiopg
import docker
import psycopg2

if sys.version_info[0] == 3 and sys.version_info[1] >= 8 and sys.platform.startswith('win'):
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())

CREATE_TABLE_Q = """
CREATE TABLE accounts (
	id serial PRIMARY KEY,
	name VARCHAR ( 50 ) NOT NULL,
	bill INT NOT NULL
);
"""

INSERT_Q_PATTERN = """
INSERT INTO accounts  (name, bill)
VALUES
{}
"""
POSTGRES_USER = 'postgres'
POSTGRES_PASSWORD = '1234'
POSTGRES_DB = 'test_speed'


def create_first_fixture():
    """Генератор фикстур, нужно менять название текстового файла и колличество записей
        НА выходе получим
        ('gslvd',5)
        ('vxzst',5)
        ('bjffa',7)
        ('lgjqg',9)
    """
    with open('fourth_fixture.txt', 'w') as f:
        for i in range(10000000):
            payload = f"('{''.join(choices(ascii_lowercase, k=5))}',{randrange(-10, 10)})\n"
            f.write(payload)


def docker_run_container():
    """Запуск контейнера с постгресом, докер уже должен быть установлен"""
    print('creating container')
    client = docker.from_env()
    container = client.containers.run(
        'postgres:9.5.10',
        environment={
            'POSTGRES_USER': POSTGRES_USER,
            'POSTGRES_PASSWORD': POSTGRES_PASSWORD,
            'POSTGRES_DB': POSTGRES_DB
        },
        ports={
            '5432/tcp': '5432'
        },
        detach=True
    )
    print(f'container {container} created')
    return container


def docker_stop_container(container):
    """Остановка контейнера после работы"""
    print('stop container')
    container.stop()
    print('container stoped')


def create_and_fill_table(fixture_name):
    """Создание новой базы в контейнере и ее заполнение данными"""
    print('connect to db')
    with psycopg2.connect(dbname=POSTGRES_DB, user=POSTGRES_USER, password=POSTGRES_PASSWORD) as conn:
        with conn.cursor() as cur:
            print('create table')
            cur.execute(CREATE_TABLE_Q)
            with open(fixture_name) as f:
                print('insert values')
                for line in f.readlines():
                    cur.execute(INSERT_Q_PATTERN.format(line))
            conn.commit()


def calc_sum_in_db():
    """Тестовая функция которая все считает в базе"""
    with psycopg2.connect(dbname=POSTGRES_DB, user=POSTGRES_USER, password=POSTGRES_PASSWORD) as conn:
        with conn.cursor() as cur:
            cur.execute('select sum(bill) from accounts where char_length(name) = 5')
            return cur.fetchone()[0]


def calc_sum_in_python():
    """Тестовая функция которая достает все строки из базы и и вычисляет сумму всех bill с помощью python"""
    with psycopg2.connect(dbname=POSTGRES_DB, user=POSTGRES_USER, password=POSTGRES_PASSWORD) as conn:
        with conn.cursor() as cur:
            cur.execute('select bill from accounts where char_length(name) = 5')
            result = cur.fetchmany(100)
            sum_of_bill = reduce(lambda x, y: x + y[0], result, 0)
            while result:
                result = cur.fetchmany(100)
                sum_of_bill += reduce(lambda x, y: x + y[0], result, 0)
    return sum_of_bill


def sync_call():
    """Тест трех синхронных вызовов расчета в базе"""
    print('start sync')
    start_time = perf_counter()
    calc_sum_in_db()
    calc_sum_in_db()
    calc_sum_in_db()
    end_time = perf_counter()
    print(f'end sync result {end_time - start_time} ms')
    return end_time - start_time


def sync_call_sum():
    """Тест трех синхронных вызовов расчета в python"""
    print('start sync call sum')
    start_time = perf_counter()
    calc_sum_in_python()
    calc_sum_in_python()
    calc_sum_in_python()
    end_time = perf_counter()
    print(f'start sync call sum {end_time - start_time} ms')
    return end_time - start_time


def thread_pool_call():
    """Тредпул трех синхронных вызовов для расчета в базе"""
    print('start thread pool')
    start_time = perf_counter()
    with ThreadPoolExecutor(max_workers=3) as executor:
        futures = [
            executor.submit(calc_sum_in_db),
            executor.submit(calc_sum_in_db),
            executor.submit(calc_sum_in_db),
        ]
        for future in as_completed(futures):
            pass
    end_time = perf_counter()
    print(f'end thread pool result {end_time - start_time} ms')
    return end_time - start_time


def thread_pool_sum_call():
    """Тредпул трех синхронных вызовов для расчета в python"""
    print('start thread pool sum')
    start_time = perf_counter()
    with ThreadPoolExecutor(max_workers=3) as executor:
        futures = [
            executor.submit(calc_sum_in_python),
            executor.submit(calc_sum_in_python),
            executor.submit(calc_sum_in_python),
        ]
        for future in as_completed(futures):
            pass
    end_time = perf_counter()
    print(f'end thread pool sum result {end_time - start_time} ms')
    return end_time - start_time


def process_pool_call():
    """Процеспул трех синхронных вызовов для расчета в базе"""
    print('start process pool')
    start_time = perf_counter()
    with ProcessPoolExecutor(max_workers=3) as executor:
        futures = [
            executor.submit(calc_sum_in_db),
            executor.submit(calc_sum_in_db),
            executor.submit(calc_sum_in_db),
        ]
        for future in as_completed(futures):
            pass
    end_time = perf_counter()
    print(f'end process pool result {end_time - start_time} ms')
    return end_time - start_time


def process_pool_sum_call():
    """Процеспул трех синхронных вызовов для расчета в python"""
    print('start process pool sum')
    start_time = perf_counter()
    with ProcessPoolExecutor(max_workers=3) as executor:
        futures = [
            executor.submit(calc_sum_in_python),
            executor.submit(calc_sum_in_python),
            executor.submit(calc_sum_in_python),
        ]
        for future in as_completed(futures):
            pass
    end_time = perf_counter()
    print(f'end process pool sum result {end_time - start_time} ms')
    return end_time - start_time


async def _run_async_q():
    """Асинхронный запрос для расчета в базе"""
    async with aiopg.create_pool(
            f'dbname={POSTGRES_DB} user={POSTGRES_USER} password={POSTGRES_PASSWORD} host=127.0.0.1') as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute('select sum(bill) from accounts where char_length(name) = 5')
                await cur.fetchone()


async def _run_async_q_sum_in_python():
    """Асинхронный запрос для расчета в python"""
    async with aiopg.create_pool(
            f'dbname={POSTGRES_DB} user={POSTGRES_USER} password={POSTGRES_PASSWORD} host=127.0.0.1') as pool:
        async with pool.acquire() as conn:
            async with conn.cursor() as cur:
                await cur.execute('select bill from accounts where char_length(name) = 5')
                result = await cur.fetchmany(100)
                sum_of_bill = reduce(lambda x, y: x + y[0], result, 0)
                while result:
                    result = await cur.fetchmany(100)
                    sum_of_bill += reduce(lambda x, y: x + y[0], result, 0)
    return result


async def _async_with_aiopg():
    """Вызов трех асинхронных функций для расчета в базе"""
    await _run_async_q()
    await _run_async_q()
    await _run_async_q()


async def _async_with_aiopg_sum_in_python():
    """Вызов трех асинхронных функций для расчета в python"""
    await _run_async_q_sum_in_python()
    await _run_async_q_sum_in_python()
    await _run_async_q_sum_in_python()


def async_aiopg_call():
    """Запуск асинхронного эвент лупа для расчета в базе"""
    print('start async aiopg run')
    start_time = perf_counter()
    ioloop = asyncio.get_event_loop()
    ioloop.run_until_complete(_async_with_aiopg())
    end_time = perf_counter()
    print(f'end async aioppg result {end_time - start_time} ms')
    return end_time - start_time


def async_aiopg_sum_call():
    """Запуск асинхронного эвент лупа для расчета в python"""
    print('start async aiopg sum run')
    start_time = perf_counter()
    ioloop = asyncio.get_event_loop()
    ioloop.run_until_complete(_async_with_aiopg_sum_in_python())
    end_time = perf_counter()
    print(f'end async aiopg sum result {end_time - start_time} ms')
    return end_time - start_time


def run(fn, fixture):
    """Функция отвечает за запуск теста,
    Создание контейнера
    На всякий случай ждем когда поднимется база в докере
    создаем и заполняем таблицу фикстурой
    запускаем тестовую функцию
    останавливаем контейнер
    пишем результат в файл
    """
    container = docker_run_container()
    sleep(15)
    create_and_fill_table(fixture)
    result = None
    try:
        result = fn()
    except Exception as e:
        print(e)
    finally:
        docker_stop_container(container)

    with open('result.txt', 'a') as f:
        f.write(f'{fixture};{fn.__name__};{result};\n')


def main():
    """Основная функция по запуску тестов"""
    print(datetime.now())
    for fixture in ['first_fixture.txt', 'second_fixture.txt',
                    'thrd_fixture.txt', 'fourth_fixture.txt',
                    'fifth_fixture.txt'
                    ]:
        for fn in [
            sync_call, sync_call, sync_call,
            sync_call_sum, sync_call_sum, sync_call_sum,
            thread_pool_call, thread_pool_call, thread_pool_call,
            thread_pool_sum_call, thread_pool_sum_call, thread_pool_sum_call,
            process_pool_call, process_pool_call, process_pool_call,
            process_pool_sum_call, process_pool_sum_call, process_pool_sum_call,
            async_aiopg_call, async_aiopg_call, async_aiopg_call,
            async_aiopg_sum_call, async_aiopg_sum_call, async_aiopg_sum_call,
        ]:
            run(fn, fixture)
    print(datetime.now())


if __name__ == '__main__':
    main()
